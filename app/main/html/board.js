// need to add a sorter for the data array.
// need to add a way to change the order of the data array, insert spaces between existing spaces, etc.

//init game data holder
var data = [];

function create_space(id, position, bg_color, font_color, name, event_type, event_script, script_args) {
	data.push({'id':id, 'position':position, 'bg_color':bg_color, 'font_color':font_color, 'name':name, 'event_type':event_type, 'event_script':event_script, 'script_args':script_args});
}

function click_parser(origin) {
	let board_index = origin.split('.');
	let script = board[board_index[0]][board_index[1]].event_script;
	let args = board[board_index[0]][board_index[1]].script_args;
	if(script == "jail") {
		jail(args);
	}
	if(script == "money") {
		money(args);
	}
	if(script == "message") {
		message(args);
	}
}

function jail() {
    message("you went to jail!");
}

function money(amount) {
    message("your money changed by " + amount);
}

function message(text) {
	let table = document.getElementById("message_table");
	let row = table.insertRow();
	let cell = row.insertCell();
	cell.innerHTML = getTimestamp() + ":  " + text;
}

function getTimestamp () {
  const pad = (n,s=2) => (`${new Array(s).fill(0)}${n}`).slice(-s);
  const d = new Date();
  
  return `${pad(d.getFullYear(),4)}-${pad(d.getMonth()+1)}-${pad(d.getDate())} ${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`;
}

const board = new Array();

message("creating board.")

create_space(0, 0, 'blue', 'black', 'Go!', 'click', 'money', 200);
create_space(1, 1, 'blue', 'black', 'Tax!', 'click', 'money', -200);
create_space(2, 2, 'gray', 'red', 'Free!', 'click', 'message', 'you are safe!');
create_space(3, 3, 'white', 'blue', 'Jail!', 'click', 'message', 'just visiting!');
create_space(4, 4, 'yellow', 'blue', 'Water!', 'click', 'message', 'one day you could buy this!');
create_space(6, 5, 'gray', 'black', 'Sadface!', 'click', 'jail', 0);
create_space(5, 6, 'gray', 'black', 'Sadface2!', 'click', 'jail', 0);
create_space(7, 7, 'gray', 'black', 'Sadface3!', 'click', 'jail', 0);
create_space(8, 8, 'gray', 'black', 'More Tax!', 'click', 'money', -100);
create_space(9, 9, 'gray', 'red', 'Free 2!', 'click', 'message', 'you are safe!');
create_space(11, 11, 'gray', 'black', 'Railroad!', 'click', 'message', 'one day you could buy this!');


//canvas
let board_x = 600;
let board_y = 600;
let board_border = '1px solid #000000';

function draw_space (x_start, x_end, y_start, y_end, bg_color, font_color, text_height, text) {
	let game_board = document.getElementById("canvas_game_board");
	let ctx = game_board.getContext("2d");
	ctx.strokeStyle = bg_color;
	ctx.strokeRect(x_start, y_start, x_end, y_end);


	ctx.font = text_height + "px Georgia";
	ctx.strokeStyle = font_color;
	ctx.strokeText(text, x_start, y_start + text_height);

}

function draw_board(board_dict=data) {
	let game_board = document.getElementById("canvas_game_board");
	//draw the canvas
	game_board.width = board_x;
	game_board.height = board_y;
	game_board.style.border = board_border;
	//set text height to 1/40th of the canvas height
	text_height = board_y / 40;
	//figure out how big the spaces should be based on how many there are per side
	let num_spaces = board_dict.length;
	let space_per_side = Math.floor(num_spaces/4);
	let extra_spaces = num_spaces % 4;
	let num_spaces_bot = space_per_side;
	if (extra_spaces > 0) {
		num_spaces_bot += 1;
		extra_spaces -= 1;
	}
	let num_spaces_left = space_per_side;
	if (extra_spaces > 0) {
		num_spaces_left += 1;
		extra_spaces -= 1;
	}
	let num_spaces_top = space_per_side;
	if (extra_spaces > 0) {
		num_spaces_top += 1;
		extra_spaces -= 1;
	}
	let num_spaces_right = space_per_side;

	space_x_max = board_x / (space_per_side + 2);
	space_y_max = board_y / (space_per_side + 2);

	//draw the spaces, first set origin
	space_x = board_x - space_x_max;
	space_y = board_y - space_y_max;
	space_index = 0;

	//draw bottom row
	move_x = -1 * space_x_max;
	move_y = 0;
	while(space_index < num_spaces_bot) {
		draw_space(space_x, (space_x + space_x_max), space_y, (space_y + space_y_max), board_dict[space_index].bg_color, board_dict[space_index].font_color, text_height, board_dict[space_index].name);
		space_x += move_x;
		space_y += move_y;
		space_index += 1;
	}
	//draw left row
	move_x = 0;
	move_y = -1 * space_y_max;
	while(space_index < num_spaces_left + num_spaces_bot) {
		draw_space(space_x, (space_x + space_x_max), space_y, (space_y + space_y_max), board_dict[space_index].bg_color, board_dict[space_index].font_color, text_height, board_dict[space_index].name);
		space_x += move_x;
		space_y += move_y;
		space_index += 1;
	}
	//draw top row
	move_x = space_x_max;
	move_y = 0;
	while(space_index < num_spaces_top + num_spaces_left + num_spaces_bot) {
		draw_space(space_x, (space_x + space_x_max), space_y, (space_y + space_y_max), board_dict[space_index].bg_color, board_dict[space_index].font_color, text_height, board_dict[space_index].name);
		space_x += move_x;
		space_y += move_y;
		space_index += 1;
	}
	//draw right row
	move_x = 0;
	move_y = space_y_max;
	while(space_index < num_spaces) {
		draw_space(space_x, (space_x + space_x_max), space_y, (space_y + space_y_max), board_dict[space_index].bg_color, board_dict[space_index].font_color, text_height, board_dict[space_index].name);
		space_x += move_x;
		space_y += move_y;
		space_index += 1;
	}
}

function clear_board() {
	let game_board = document.getElementById("canvas_game_board");
	ctx = game_board.getContext('2d');
	ctx.clearRect(0, 0, game_board.width, game_board.height);
}

draw_board();
